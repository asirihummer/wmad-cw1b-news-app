import 'package:flutter/material.dart';
import 'package:wmad_cw1b_news_app/helper/newsdatahelper.dart';
import '../model/newsmodel.dart';
import 'package:cached_network_image/cached_network_image.dart';


class ForeignBusinessNewsPage extends StatefulWidget {

  @override
  State<ForeignBusinessNewsPage> createState() => _ForeignBusinessNewsPageState();
}

class _ForeignBusinessNewsPageState extends State<ForeignBusinessNewsPage> {

  bool _loading = true;
  List<NewsArticle> foreignbusinessnewslist = [];

  getNews() async {
    News businessnews = News();
    await businessnews.getForeignNews(['uk', 'ca', 'au', 'cn', 'jp', 'ru'], 'business');
    foreignbusinessnewslist = businessnews.foreignnewsList;
    setState(() {
      _loading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    getNews();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme
            .of(context)
            .colorScheme
            .primary,
        title: Text('Foreign Business News',style: TextStyle(color: Colors.white),),
      ),
      body: _loading ? Center(
        child: CircularProgressIndicator(

        ),
      ): SingleChildScrollView(
        child: Container(
          color: Colors.white,
          child: Column(
            children: <Widget>[
              Container(
                child: ListView.builder(
                  itemCount:  foreignbusinessnewslist.length,
                  physics: ClampingScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (context, index) {

                    return NewsTemplate(
                      urlToImage: foreignbusinessnewslist[index].urlToImage,
                      title: foreignbusinessnewslist[index].title,
                      description: foreignbusinessnewslist[index].description,
                    );
                  } ,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class NewsTemplate extends StatelessWidget {

  String title, description, urlToImage;
  NewsTemplate({required this.title, required this.description, required this.urlToImage});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.all(16),
      child: Column(
        children: <Widget>[
          ClipRRect(
              borderRadius: BorderRadius.circular(6),
              child: CachedNetworkImage(imageUrl: urlToImage, width: 380, height: 200, fit: BoxFit.cover,)),
          SizedBox(height: 8),
          Text(title, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0, color: Colors.black),),
          SizedBox(height: 8),
          Text(description, style: TextStyle( fontSize: 15.0, color: Colors.grey[800]),),
        ],
      ),
    );
  }
}