import 'package:flutter/material.dart';
import 'package:wmad_cw1b_news_app/views/foreign_business_news.dart';
import 'package:wmad_cw1b_news_app/views/foreign_entertainment_news.dart';
import 'package:wmad_cw1b_news_app/views/foreign_health_news.dart';
import 'package:wmad_cw1b_news_app/views/foreign_science_news.dart';
import 'package:wmad_cw1b_news_app/views/foreign_sports_news.dart';

class ForeignNewsCategories extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme
            .of(context)
            .colorScheme
            .primary,
        title: Text('Foreign News',style: TextStyle(color: Colors.white),),
      ),
      body: Center(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.all(15),
                child: SizedBox(
                  height: 120,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => ForeignBusinessNewsPage()),
                      );
                    },
                    clipBehavior: Clip.antiAlias,
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                      ),
                      padding: EdgeInsets.zero,
                    ),
                    child: Ink.image(
                      image: AssetImage('assets/images/business.PNG'),
                      fit: BoxFit.cover,
                      colorFilter: ColorFilter.mode(
                        Colors.black.withOpacity(0.4),
                        BlendMode.darken,
                      ),
                      child: Center(
                        child: Text(
                          'Business',
                          style: TextStyle(fontSize: 34, color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(15),
                child: SizedBox(
                  height: 120,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => ForeignEntertainmentNewsPage()),
                      );
                    },
                    clipBehavior: Clip.antiAlias,
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                      ),
                      padding: EdgeInsets.zero,
                    ),
                    child: Ink.image(
                      image: AssetImage('assets/images/entertainment.PNG'),
                      fit: BoxFit.cover,
                      colorFilter: ColorFilter.mode(
                        Colors.black.withOpacity(0.4),
                        BlendMode.darken,
                      ),
                      child: Center(
                        child: Text(
                          'Entertainment',
                          style: TextStyle(fontSize: 34, color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(15),
                child: SizedBox(
                  height: 120,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => ForeignHealthNewsPage()),
                      );
                    },
                    clipBehavior: Clip.antiAlias,
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                      ),
                      padding: EdgeInsets.zero,
                    ),
                    child: Ink.image(
                      image: AssetImage('assets/images/health.PNG'),
                      fit: BoxFit.cover,
                      colorFilter: ColorFilter.mode(
                        Colors.black.withOpacity(0.4),
                        BlendMode.darken,
                      ),
                      child: Center(
                        child: Text(
                          'Health',
                          style: TextStyle(fontSize: 34, color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(15),
                child: SizedBox(
                  height: 120,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => ForeignScienceNewsPage()),
                      );
                    },
                    clipBehavior: Clip.antiAlias,
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                      ),
                      padding: EdgeInsets.zero,
                    ),
                    child: Ink.image(
                      image: AssetImage('assets/images/science.PNG'),
                      fit: BoxFit.cover,
                      colorFilter: ColorFilter.mode(
                        Colors.black.withOpacity(0.4),
                        BlendMode.darken,
                      ),
                      child: Center(
                        child: Text(
                          'Science',
                          style: TextStyle(fontSize: 34, color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(15),
                child: SizedBox(
                  height: 120,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => ForeignSportsNewsPage()),
                      );
                    },
                    clipBehavior: Clip.antiAlias,
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                      ),
                      padding: EdgeInsets.zero,
                    ),
                    child: Ink.image(
                      image: AssetImage('assets/images/sports.PNG'),
                      fit: BoxFit.cover,
                      colorFilter: ColorFilter.mode(
                        Colors.black.withOpacity(0.4),
                        BlendMode.darken,
                      ),
                      child: Center(
                        child: Text(
                          'Sports',
                          style: TextStyle(fontSize: 34, color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}