class NewsArticle {
  String title;
  String description;
  String url;
  String urlToImage;

  NewsArticle({required this.title, required this.description, required this.url, required this.urlToImage});
}