import 'package:flutter/material.dart';
import 'views/local_news_categories.dart';
import 'views/foreign_news_categories.dart';

void main() {
  runApp(const NEWSApp());
}

class NEWSApp extends StatelessWidget {
  const NEWSApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo NEWS App',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.red),
        useMaterial3: true,
      ),
      home: const NEWSAppHomePage(title: 'NEWS Partner'),
    );
  }
}

class NEWSAppHomePage extends StatefulWidget {
  const NEWSAppHomePage({super.key, required this.title});

  final String title;

  @override
  State<NEWSAppHomePage> createState() => _NEWSAppHomePageState();
}

class _NEWSAppHomePageState extends State<NEWSAppHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme
            .of(context)
            .colorScheme
            .primary,
        title: Text(widget.title,style: TextStyle(color: Colors.white),),
      ),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/NEWSBACK.png'),
            fit: BoxFit.cover,
          ),
        ),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.all(30),
                child: SizedBox(
                  height: 200,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => LocalNewsCategories()),
                      );
                    },
                    clipBehavior: Clip.antiAlias,
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                      ),
                      padding: EdgeInsets.zero,
                    ),
                    child: Ink.image(
                      image: AssetImage('assets/images/LNEWS.png'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
              SizedBox(height: 16),
              Container(
                margin: EdgeInsets.all(30),
                child: SizedBox(
                  height: 200,
                  child: ElevatedButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => ForeignNewsCategories()),
                      );
                    },
                    clipBehavior: Clip.antiAlias,
                    style: ElevatedButton.styleFrom(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(18.0),
                      ),
                      padding: EdgeInsets.zero,
                    ),
                    child: Ink.image(
                      image: AssetImage('assets/images/FNEWS.PNG'),
                      fit: BoxFit.cover,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
