import 'dart:convert';
import 'package:wmad_cw1b_news_app/model/newsmodel.dart';
import 'package:http/http.dart' as http;

class News {

  List<NewsArticle> localnewslist = [];
  List<NewsArticle> foreignnewsList = [];

  Future<void> getLocalNews(String country, String category) async {

    final Uri uri = Uri.parse('https://newsapi.org/v2/top-headlines?country=$country&category=$category&apiKey=d4af9748368b4fe688b17125b5245463');
    var response = await http.get(uri);
    var jsonData = jsonDecode(response.body);

    if (jsonData['status'] == 'ok') {

      jsonData['articles'].forEach((element) {


        if (element['urlToImage']!=null && element['description']!=null) {

          NewsArticle newsArticleModel = NewsArticle(
            title: element['title'],
            urlToImage: element['urlToImage'],
            description: element['description'],
            url: element['url'],
          );

          localnewslist.add(newsArticleModel);

        }
      });
    }
  }

  Future<void> getForeignNews(List<String> countries, String category) async {
    for (var country in countries) {
      final Uri uri = Uri.parse('https://newsapi.org/v2/top-headlines?country=$country&category=$category&apiKey=d4af9748368b4fe688b17125b5245463');
      var response = await http.get(uri);
      var jsonData = jsonDecode(response.body);

      if (jsonData['status'] == 'ok') {

        jsonData['articles'].forEach((element) {


          if (element['urlToImage']!=null && element['description']!=null) {

            NewsArticle newsArticleModel = NewsArticle(
              title: element['title'],
              urlToImage: element['urlToImage'],
              description: element['description'],
              url: element['url'],
            );

            foreignnewsList.add(newsArticleModel);

          }
        });
      }
    }
  }
}